import {NODES, NODES_DESCRIPTIONS, NODES_LABELS} from "./constants.ts";


function Termin(props: {title: string, description: string}) {
    const {title, description} = props;
    return (
        <div>
            <b>{title}</b>
            <div>{description}</div>
        </div>
    )
}

export const nodes = [
    {id: NODES.devOps, position: {x: 0, y: 0}},
    {id: NODES.ciCd, position: {x: 0, y: 200}},
    {id: NODES.ci, position: {x: -200, y: 400}},
    {id: NODES.cd, position: {x: 200, y: 400}},
    {id: NODES.release, position: {x: 200, y: 700}},
    {id: NODES.deploy, position: {x: 550, y: 700}},
    {id: NODES.production, position: {x: -150, y: 700}},
    {id: NODES.ciSystems, position: {x: -550, y: 700}},
    {id: NODES.jenkins, position: {x: -100, y: 1100}},
    {id: NODES.gitlabCI, position: {x: -700, y: 1100}},
    {id: NODES.gitlab, position: {x: -1100, y: 700}},
    {id: NODES.vcs, position: {x: -1300, y: 300}},
    {id: NODES.branch, position: {x: -1500, y: 700}},
    {id: NODES.pullRequest, position: {x: -1100, y: 1100}},
    {id: NODES.openSource, position: {x: -1500, y: 1100}},
    {id: NODES.build, position: {x: -1200, y: 1550}},
    {id: NODES.artifacts, position: {x: -1200, y: 1800}},
    {id: NODES.stage, position: {x: -500, y: 1550}},
    {id: NODES.job, position: {x: -500, y: 1800}},
    {id: NODES.pipeline, position: {x: -700, y: 1350}},
    {id: NODES.runner, position: {x: -100, y: 1550}},
    {id: NODES.docker, position: {x: -850, y: 1550}},
]
    .map(n => ({
        ...n,
        data: {label: <Termin title={NODES_LABELS[n.id]} description={NODES_DESCRIPTIONS[n.id]}/>},
        style: {width: '300px'}
    }))
