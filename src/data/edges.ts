import {NODES} from 'src/data/constants';

export const EDGES = [
    {id: '1', source: NODES.devOps, target: NODES.ciCd},
    {id: '2', source: NODES.ciCd, target: NODES.ci},
    {id: '2', source: NODES.ciCd, target: NODES.cd},
    {id: '3', source: NODES.cd, target: NODES.deploy},
    {id: '4', source: NODES.cd, target: NODES.release},
    {id: '5', source: NODES.cd, target: NODES.production},
    {id: '6', source: NODES.ci, target: NODES.ciSystems},
    {id: '7', source: NODES.ciSystems, target: NODES.gitlabCI},
    {id: '8', source: NODES.ciSystems, target: NODES.jenkins},
    {id: '9', source: NODES.gitlab, target: NODES.gitlabCI},
    {id: '10', source: NODES.gitlab, target: NODES.openSource},
    {id: '11', source: NODES.gitlab, target: NODES.pullRequest},
    {id: '12', source: NODES.vcs, target: NODES.gitlab},
    {id: '13', source: NODES.vcs, target: NODES.branch},
    {id: '14', source: NODES.gitlabCI, target: NODES.pipeline},
    {id: '15', source: NODES.pipeline, target: NODES.docker},
    {id: '16', source: NODES.pipeline, target: NODES.runner},
    {id: '17', source: NODES.pipeline, target: NODES.stage},
    {id: '18', source: NODES.stage, target: NODES.job},
    {id: '19', source: NODES.pipeline, target: NODES.build},
    {id: '20', source: NODES.build, target: NODES.artifacts}
];
